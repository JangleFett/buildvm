#!/usr/bin/python3.7

import boto3
import sys

if len(sys.argv) < 3:
    print("SYNTAX: "+sys.argv[0]+"<domain> <hostname> <pointstoname>")
    sys.exit(1)

ourDomain=sys.argv[1]
hostname=sys.argv[2]
pointsto=sys.argv[3]

client = boto3.client('route53')

zones = client.list_hosted_zones_by_name()
if not zones or len(zones['HostedZones']) == 0:
    raise Exception("Could not find DNS zone to update")
for zone in zones['HostedZones']:
    #print(zone)
    if zone['Name'] == ourDomain:
        zone_id = zone['Id']

print("Zone ID: "+zone_id)

try:
    response = client.change_resource_record_sets(
    HostedZoneId=zone_id,
    ChangeBatch= {
            'Comment': 'Adding steve as DNS',
            'Changes': [
            {
                'Action': 'CREATE',
                'ResourceRecordSet': {
                'Name': "steve.grads.al-labs.co.uk",
                'Type': 'CNAME',
                'TTL': 300,
                'ResourceRecords': [{'Value': 'jenkins.grads.al-labs.co.uk'}]
            }
        }]
    })

    print(response)

except Exception as e:
	print(e)
