import boto3
 
client = boto3.client('route53')
hostedZoneId = 'Z2U00Q95U7EKEA'
 
response = client.change_resource_record_sets(
    HostedZoneId = hostedZoneId,
    ChangeBatch={
        'Comment': 'YourName added DNS record',
        'Changes': [
            {
                'Action': 'CREATE',
                'ResourceRecordSet': {
                    'Name': 'steve.grads.al-labs.co.uk',
                    'Type': 'CNAME',
                    'TTL': 60,
                    'ResourceRecords': [
                        {
                            'Value': "jenkins.grads.al-labs.co.uk"
                        },
                        ],
                    }
            },
            ]
    }
)
 
 
print("DNS record status %s "  % response['ChangeInfo']['Status'])
print("DNS record response code %s " % response['ResponseMetadata']['HTTPStatusCode'])
